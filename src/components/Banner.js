import React from 'react'

const Banner = (props) => (
    <section id="banner" className="major">
        <div className="inner">
            <header className="major">
                <h1>David McFadzean, P.Eng.</h1>
            </header>
            <div className="content">
                <p>Veteran software engineer/architect/manager</p>
                <ul className="actions">
                    <li><a href="#one" className="button next scrolly">Welcome</a></li>
                </ul>
            </div>
        </div>
    </section>
)

export default Banner
